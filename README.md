# Reactor-Engineer

Hi there EtowTheOfficeCat Here! Today i´ll present you my current VR Game project Titled Reactor Engineer

![](https://media.giphy.com/media/jUiko9b4A2rfNAB3eh/giphy.gif)

:grey_exclamation: **game is not finished, Its in Planing stages still** :grey_exclamation:

# About the project
I decided to make this little game because I love Engineering games in which you have to repair and manage systems, but also to improuve my VR programming mainly in creating different interactions ( Pushing buttons / Opening panels / exchanging objects / repairing Objects etc...) 

The Game idea is rather simple. The Player has to manage a reactor, producting energy and repairing its components.

# Concept Art (updated 22.10.19)

![Alt Text](https://cdna.artstation.com/p/assets/images/images/021/431/832/large/paul-mabon-side.jpg?1571671028)

![Alt Text](https://cdna.artstation.com/p/assets/images/images/021/431/700/large/paul-mabon-reactor.jpg?1571670715)

This Should give you a more clear Idea of how the game will look like. This there is the Reactor Control Panel from where the player will do most of the interaction with the reactor output and the reactor temperature.

as you can see it has a large array of buttons for the player to push as well as places for UI to inform the player about the reactors current status. 

All Programms and Assets are my own creations!:)

I will Hopefully Upload more about this project soon!!Stay Tuned :D

